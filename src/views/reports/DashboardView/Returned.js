import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Grid,
  LinearProgress,
  Typography,
  makeStyles,
  colors
} from '@material-ui/core';
import InsertChartIcon from '@material-ui/icons/InsertChartOutlined';
import CircularProgress from '../../../components/Progress';

const useStyles = makeStyles(() => ({
  root: {
    height: '100%'
  },
  avatar: {
    backgroundColor: colors.orange[600],
    height: 56,
    width: 56
  }
}));

const ReturnedUsers = ({ className, ...rest }) => {
  const classes = useStyles();

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
    >
    <CardContent>
      <Grid
        container
        justify="space-between"
        spacing={3}
      >
        <Grid item>
          <CircularProgress value={68} />
        </Grid>
        <Grid item>
          <Typography
            color="textSecondary"
            gutterBottom
            variant="h6"
          >
            Returned
          </Typography>
          <Typography
            color="textPrimary"
            variant="h3"
          >
            32,592
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
    </Card>
  );
};

ReturnedUsers.propTypes = {
  className: PropTypes.string
};

export default ReturnedUsers;
