import React from 'react';
import DashboardLayout from 'src/layouts/DashboardLayout';
import DashboardView from 'src/views/reports/DashboardView';


const routes = [
  {
    path: '/',
    element: <DashboardLayout />,
    children: [
      { path: '', element: <DashboardView /> }
    ]
  }
];

export default routes;
